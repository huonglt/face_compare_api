import psycopg2

from Object import User
from util import load_config

Config = load_config()


class DatabaseConsumer:
    def __init__(self):
        conn = psycopg2.connect(host=Config['postgres']['host'],
                                database=Config['postgres']['database'],
                                user=Config['postgres']['user'],
                                password=Config['postgres']['password'])
        self.cursor = conn.cursor()

    def consume(self):
        select_query = "select * from users"
        self.cursor.execute(select_query)
        while True:
            row = self.cursor.fetchone()
            if row is None:
                break
            user_id, cmnd_id, passport_id = row
            user = User(user_id=user_id)
            yield user


if __name__ == '__main__':
    database = DatabaseConsumer()
