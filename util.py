import os
import yaml


def load_config():
    """
    load config from config.yaml file
    :return:
    """
    yml_file = open(os.path.join(os.path.dirname(__file__), 'config', 'config.yaml'), 'r', encoding="utf-8")
    Config = yaml.load(yml_file, Loader=yaml.FullLoader)
    yml_file.close()
    return Config
