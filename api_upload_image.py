import os
import flask
from flask import Flask, flash, request, redirect, url_for, jsonify
from werkzeug.utils import secure_filename
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.exceptions import BadRequest

from Object import Passport, CMND
from face_processing import verify, img_to_encoding, verify_2_img
from util import load_config
from Database_connector import UserRepository, CMNDRepository, PassportRepository

Config = load_config()


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


app = flask.Flask(__name__)
app.config["DEBUG"] = True
cwd = os.getcwd()

ALLOWED_EXTENSIONS = Config["api_upload_image"]["extension_allowed"]
ALLOWED_TYPE_FILE = Config['api_upload_image']["type_allowed"]

app.config['UPLOAD_FOLDER'] = Config['api_upload_image']["upload_folder"]
app.config['ALLOWED_TYPE_FILE'] = ALLOWED_TYPE_FILE


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def allowed_type_file(type):
    return type in ALLOWED_TYPE_FILE


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    # response = jsonify(error.to_dict())
    response = {"error-name": error.name, "error-description": error.description}
    response["status_code"] = error.code
    return response


@app.route(Config["api_upload_image"]["url"], methods=Config["api_upload_image"]["method"])
def upload_file():
    dist = 0
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        user_id = request.form.get('userId')
        type = request.form.get('type')
        print('TEST', type)

        if (allowed_type_file(type)):
            # if user does not select file, browser also
            # submit an empty part without filename
            if file.filename == '':
                flash('No selected file')
                return redirect(request.url)
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                if not user_id:
                    return {"status_code": 400, "error": "No user_id parameter found!"}
                folder_path = os.path.join(Config['api_upload_image']["upload_folder"], f"uploads_{type}", user_id)
                if not os.path.exists(folder_path):
                    os.makedirs(folder_path)
                file_path = os.path.join(folder_path, filename)
                file.save(file_path)

                # check user tồn tại chưa nếu chưa thì trả về lỗi
                ur = UserRepository()
                cmndr = CMNDRepository()
                pr = PassportRepository()
                # if not ur.check_user_id(user_id=user_id):
                #     return {"status_code": 402, "des": "user_id not found", "dist": None}
                if type == "avatar":
                    ur.update_avatar_path(user_id=user_id, path=file_path)

                    if pr.check_passport(user_id=user_id):
                        path = pr.get_passport_path(user_id=user_id)
                        pr_img = img_to_encoding(path)
                        img = img_to_encoding(file_path)
                        dist = verify_2_img(img, pr_img)
                        ur.update_iskyc(user_id=user_id)
                        return {"status_code": 200, "dist": str(dist)}
                    elif cmndr.check_cmnd(user_id=user_id):
                        path = cmndr.get_cmnd_path(user_id=user_id)
                        pr_img = img_to_encoding(path)
                        img = img_to_encoding(file_path)
                        dist = verify_2_img(img, pr_img)

                        return {"status_code": 200, "dist": str(dist)}
                    else:
                        return {"status_code": 202, "des": "No information to compare found!", "dist": None}
                else:
                    if type == 'passport':
                        if pr.check_passport(user_id):
                            pr.update_passport_path(user_id, file_path)
                        else:
                            pr.create(Passport(user_id=user_id, passport_path=file_path))
                        return {"status_code": 200}

                    if type == 'identity_card_front':
                        if cmndr.check_cmnd(user_id):
                            cmndr.update_cmnd_font_path(user_id, file_path)
                        else:
                            cmndr.create(CMND(user_id=user_id, cmnd_font_path=file_path))
                        return {"status_code": 200}
                    if type == 'identity_card_back':
                        if cmndr.check_cmnd(user_id):
                            cmndr.update_cmnd_back_path(user_id, file_path)
                        else:
                            cmndr.create(CMND(user_id=user_id, cmnd_back_path=file_path))
                        return {"status_code": 200}

                # nếu type = avatar
                # check passport của user tồn tại không -> nếu có compare
                # check cmnd font của user tồn tại không -> nếu có compare
                # update avatar path
                # nếu type != avatar
                # update path vào type
        else:
            return handle_invalid_usage(BadRequest('File extension not allowed'))
        return {"status_code": 200, "dist": str(dist)}
    else:
        return handle_invalid_usage(BadRequest('File extension not allowed'))


app.add_url_rule('/uploads/<filename>', 'uploaded_file',
                 build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/uploads': app.config['UPLOAD_FOLDER']
})
app.run(host=Config["api_upload_image"]["host"], port=Config['api_upload_image']["port"])
