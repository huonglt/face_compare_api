import os

import ArcFace
from mtcnn.mtcnn import MTCNN
import cv2
from tensorflow.keras.preprocessing import image
import numpy as np
from util import load_config

model = ArcFace.loadModel()
face_detector = MTCNN()
Config = load_config()


def detect_face(img):
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # mtcnn expects RGB but OpenCV read BGR
    detections = face_detector.detect_faces(img_rgb)
    detection = detections[0]
    x, y, w, h = detection["box"]
    detected_face = img[int(y):int(y + h), int(x):int(x + w)]
    return detected_face


def preprocess_face(img, target_size=(112, 112)):
    img = cv2.imread(img)
    img = detect_face(img)
    img = cv2.resize(img, target_size)
    img_pixels = image.img_to_array(img)
    img_pixels = np.expand_dims(img_pixels, axis=0)
    img_pixels /= 255  # normalize input in [0, 1]
    return img_pixels


def img_to_encoding(path):
    img = preprocess_face(path)
    return model.predict(img)[0]



def get_database(user_id, type):
    database = {}
    check = False
    folder_tmp = os.listdir(os.getcwd())
    folders = []
    for f in folder_tmp:
        if os.path.isdir(f) and "uploads" in f and f"uploads_{type}" != f:
            folders.append(f)
    for folder in folders:
        folder_passport_path = os.path.join(os.getcwd(), folder)
        user_ids = os.listdir(folder_passport_path)
        for u in user_ids:
            if u == user_id:
                database[u] = img_to_encoding(
                os.path.join(folder_passport_path, u, os.listdir(os.path.join(folder_passport_path, u))[0]))
        if len(database) != 0:
            check = True
            break
    if check:
        return database
    else:
        return None


def EuclideanDistance(source_representation, test_representation):
    euclidean_distance = source_representation - test_representation
    euclidean_distance = np.sum(np.multiply(euclidean_distance, euclidean_distance))
    euclidean_distance = np.sqrt(euclidean_distance)
    return euclidean_distance


verification_threshhold = 4.4


def verify(image_path, identity, database):
    # Step 1: Compute the encoding for the image. Use img_to_encoding()
    encoding = img_to_encoding(image_path)

    # Step 2: Compute distance with identity's image
    dist = EuclideanDistance(encoding, database[identity])

    # Step 3: Open the door if dist < verification_threshhold, else don't open
    if dist < verification_threshhold:
        print("It's " + str(identity) + ", welcome!")
    else:
        print("It's not " + str(identity) + ", please go away")

    return dist

def verify_2_img(image_1, image_2):
    dist = EuclideanDistance(image_1, image_2)
    return dist


# print(verify("images/ali.jpg", "alireza", database))
