class User:
    def __init__(self, user_id=None, avatar_path=None):
        self.user_id = user_id
        self.avatar_path = avatar_path


class CMND:
    def __init__(self, cmnd_number=None, cmnd_font_path=None, cmnd_back_path=None, user_id=None):
        self.cmnd_number = cmnd_number
        self.cmnd_font_path = cmnd_font_path
        self.cmnd_back_path = cmnd_back_path
        self.user_id = user_id


class Passport:
    def __init__(self, passport_number=None, passport_path=None, user_id=None):
        self.passport_number = passport_number
        self.passport_path = passport_path
        self.user_id = user_id
