import logging

import psycopg2
from Object import User, Passport, CMND
from util import load_config

Config = load_config()

conn = psycopg2.connect(host=Config['postgres']['host'],
                        database=Config['postgres']['database'],
                        user=Config['postgres']['user'],
                        password=Config['postgres']['password'])

cursor = conn.cursor()
check_user = f'SELECT * FROM users ' \
             f'WHERE id = %s'
# insert_user_query = """INSERT INTO users (userId, cmnd_number, passport_number)
#                        VALUES  (%s,%s,%s) RETURNING id"""
update_avatar_query = """UPDATE users SET avatarPath = %s
                       where id = %s returning id"""
update_iskyc_query = """UPDATE users SET isKyc = True
                       where id = %s returning id"""

check_cmnd_number = f'SELECT * FROM userIdentity ' \
                    f'WHERE cmndNumber = %s'
insert_cmnd_query = """INSERT INTO userIdentity (cmndNumber, cmndFontPath, cmndBackPath, userId)
                       VALUES  (%s,%s,%s, %s)"""
update_cmnd_font_query = """UPDATE userIdentity SET cmndFontPath = %s
                       where userId = %s"""
update_cmnd_back_query = """UPDATE userIdentity SET cmndBackPath = %s
                       where userId = %s"""
check_cmnd_query = """select * from userIdentity
                       where userId = %s"""
get_cmnd_path_query = """select cmndFontPath from userIdentity
                       where userId = %s"""

check_passport_number = f'SELECT * FROM userIdentity ' \
                        f'WHERE passportNumber = %s'
insert_passport_query = """INSERT INTO userIdentity (passportNumber, passportPath, userId)
                       VALUES  (%s,%s, %s) RETURNING id"""
update_passport_query = """UPDATE userIdentity SET passportPath = %s
                       where userId = %s RETURNING id"""
check_passport_query = """select * from userIdentity
                       where userId = %s"""

get_passport_path_query = """select passportPath from userIdentity
                       where userId = %s"""


def log_init_db(error: str):
    logging.basicConfig(filename="../tmp/init_database_log.log", filemode="a",
                        format='%(asctime)s - %(message)s', level=logging.INFO)
    logging.info(error)


class UserRepository:
    def check_user_id(self, user_id):
        cursor.execute(check_user, (user_id,))
        row = cursor.fetchone()[0]
        if row is not None:
            return True
        else:
            return False

    def update_avatar_path(self, user_id, path):
        id = cursor.execute(update_avatar_query, (path, user_id))
        conn.commit()
        return id
    def update_iskyc(self, user_id):
        id = cursor.execute(update_iskyc_query, (user_id,))
        conn.commit()
        return id


class CMNDRepository:
    @staticmethod
    def create(cmnd: CMND):
        cmnd_number = cmnd.cmnd_number
        cmnd_font_path = cmnd.cmnd_font_path
        cmnd_back_path = cmnd.cmnd_back_path
        user_id = cmnd.user_id
        data = (cmnd_number, cmnd_font_path, cmnd_back_path, user_id)

        cursor.execute(check_cmnd_number, (cmnd.cmnd_number,))
        if not cursor.fetchone():
            # cmnd_number, cmnd_font_path, cmnd_back_path, user_id
            cursor.execute(insert_cmnd_query, data)
            conn.commit()

    def check_cmnd(self, user_id):
        cursor.execute(check_cmnd_query, (user_id,))
        row = cursor.fetchone()
        if row is not None:
            return True
        else:
            return False

    def update_cmnd_font_path(self, user_id, path):
        id = cursor.execute(update_cmnd_font_query, (path, user_id))
        conn.commit()
        return id

    def update_cmnd_back_path(self, user_id, path):
        id = cursor.execute(update_cmnd_back_query, (path, user_id))
        conn.commit()
        return id

    def get_cmnd_path(self, user_id):
        cursor.execute(get_cmnd_path_query, (user_id,))
        return cursor.fetchone()[0]


class PassportRepository:
    @staticmethod
    def create(passport: Passport):
        passport_number = passport.passport_number
        user_id = passport.user_id
        passport_path = passport.passport_path

        data = (passport_number, passport_path, user_id)
        cursor.execute(check_passport_number, (passport.passport_number,))
        if not cursor.fetchone():
            # passport_number, passport_path, user_id
            cursor.execute(insert_passport_query, data)
            conn.commit()
        else:
            # passport_number, passport_path, user_id
            cursor.execute(update_passport_query, data)
            conn.commit()

    def check_passport(self, user_id):
        cursor.execute(check_passport_query, (user_id,))
        row = cursor.fetchone()
        if row is not None:
            return True
        else:
            return False

    def update_passport_path(self, user_id, path):
        id = cursor.execute(update_passport_query, (path, user_id))
        conn.commit()
        return id

    def get_passport_path(self, user_id):
        cursor.execute(get_passport_path_query, (user_id,))
        return cursor.fetchone()[0]
